# Ansible Role Matomo

Simple Ansible role to install and configure Matomo in my self-hosting context

The `matomo_mysql_password` and `matomo_site_to_parse` variables need to be set.

The following optional variables can be overriden (defaulting to "matomo") :
*  `matomo_directory_name`
*  `matomo_mysql_database_name`
*  `matomo_mysql_username`
*  `matomo_apache_site_name`
*  `matomo_apache_site_alias`

If you want to enable GeoIP database, you need to create an account on https://www.maxmind.com, create a license key (for GeoIP update>=3.1.1) and set the following variables with the generated credentials :
*  `matomo_maxmind_account_id`
*  `matomo_maxmind_license_key`
